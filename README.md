# Highlights
1. Multiple movies and rooms are supported
2. Web client (AngularJS)


# Run
	git clone https://igorepst@bitbucket.org/igorepst/bookyst.git
	cd bookyst/init
	
* Ensure Java 8 is installed and is in PATH (run `java -version`)

* Ensure MySQL [or compatible] daemon is up and running. See [these](http://www.ntu.edu.sg/home/ehchua/programming/sql/MySQL_HowTo.html#zz-3.1) insructions for Windows or your Linux distribution's documentation

* Run script to prepopulate the DB:
    * Linux: Ensure that user & password are right in `init.sh`

	`chmod +x init.sh && ./init.sh`
	
    * Windows: Ensure that user & password are right in `init.bat` & run it.

* `cd ..`

* Run script to build and execute the app (*no need* to install Gradle/Tomcat/etc.):
    * Linux: `chmod +x gradlew && ./gradlew build && ./gradlew bootRun`
    * Windows: `gradlew build && gradlew bootRun`
  
* For subsequent runs it's enough to execute `java -jar build\libs\bookyst-0.0.1-SNAPSHOT.jar` or just double click this jar

* Go to http://localhost:8080 and navigate from there or [use curl](#curl-usage)
 

# Build env
1. [Arch Linux](https://www.archlinux.org) 64 bit
2. [Maria DB](https://mariadb.com) - Arch Linux's default implementation of MySQL
3. [Eclipse](http://www.eclipse.org/) for J2EE, Mars.1 Release (4.5.1), with [Spring Tools Suite](https://spring.io/tools/sts/all) 3.7.2 Release + [Buildship Gradle Integration](http://marketplace.eclipse.org/content/buildship-gradle-integration) 1.0
4. [Spring MVC](http://spring.io/) 4.2.4 + [Hibernate](http://hibernate.org) 4.3.11
5. [Gradle](http://gradle.org/) 2.9
6. [MySQL Workbench](https://www.mysql.com/products/workbench/) 6.3.6
7. [Curl](https://curl.haxx.se/) 7.47.0. 
8. Firefox 44.0.2
9. AngularJS 1.5.0 for the web client


# Curl usage
1. List movies:

    `curl http://localhost:8080/movies`
	
	Response:
	
    `[{"id":4,"movieName":"Rambo"},{"id":5,"movieName":"Rambo 2"},`
	`{"id":6,"movieName":"Rambo 3"},{"id":1,"movieName":"The Godfather"},`
	`{"id":2,"movieName":"The Godfather 2"},{"id":3,"movieName":"The Godfather 3"}]`
	
2. List sessions for specific movie:

    `curl --header "Content-type: application/json" --header "Accept: application/json" \`
    `--data '{"id": 3}' http://localhost:8080/sessions`
	
	Response:
	
    `[{"id":5,"startTime":"13:00"},{"id":6,"startTime":"16:00"},`
	`{"id":7,"startTime":"19:00"},{"id":8,"startTime":"22:00"}]`
	
3. List occupied seats for specific session:

    `curl --header "Content-type: application/json" --header "Accept: application/json" \`
    `--data '{"id": 11}' http://localhost:8080/occupied`
	
	Respose:
	
    `{"id":11,"chairs":[52,66],"cols":15,"rows":8}`
	
4. Buy tickets:

    `curl --header "Content-type: application/json" --header "Accept: application/json" \`
    `--data '{"id": 24, "chairs": [ 7 ]}' http://localhost:8080/buy`


# Test env
1. Windows 7 64 bit
2. [MySQL Community Server](http://dev.mysql.com/downloads/mysql/) 5.7.11
3. Chrome 48


# Sources of information (part of, in no particular order)
1. [Spring 4 MVC+Hibernate 4+MySQL+Maven integration example using annotations](http://websystique.com/springmvc/spring-4-mvc-and-hibernate4-integration-example-using-annotations/)
2. [Spring Initializr](http://start.spring.io/)
3. [Hibernate annotations](https://docs.jboss.org/hibernate/stable/annotations/reference/en/html_single/#entity-mapping-entity)
4. [Mapping the response body with the @ResponseBody annotation](http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html#mvc-ann-responsebody)
5. [DataSourceProperties.java](https://github.com/spring-projects/spring-boot/blob/master/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java)
6. [Create board with AngularJS](http://plnkr.co/edit/tM9M28Gq9Vzj2ecLVsHY?p=preview)
