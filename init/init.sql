drop database if exists bookyst;

create database bookyst;
use bookyst;

create table `movies` (
`id` int NOT NULL AUTO_INCREMENT,
`movie_name` varchar(255) NOT NULL,
primary key (`id`)
) CHARSET=utf8;
insert into `movies` (`movie_name`) VALUES("The Godfather");
insert into `movies` (`movie_name`) VALUES("The Godfather 2");
insert into `movies` (`movie_name`) VALUES("The Godfather 3");
insert into `movies` (`movie_name`) VALUES("Rambo");
insert into `movies` (`movie_name`) VALUES("Rambo 2");
insert into `movies` (`movie_name`) VALUES("Rambo 3");

create table `room` (
`id` int NOT NULL AUTO_INCREMENT,
`room_num` int NOT NULL,
`seats_rows` int NOT NULL,
`seats_cols` int NOT NULL,
primary key (`id`)
) CHARSET=utf8;
insert into `room` (`room_num`, `seats_rows`, `seats_cols`) VALUES("1", 10, 15);
insert into `room` (`room_num`, `seats_rows`, `seats_cols`) VALUES("2", 8, 15);
insert into `room` (`room_num`, `seats_rows`, `seats_cols`) VALUES("3", 12, 10);
insert into `room` (`room_num`, `seats_rows`, `seats_cols`) VALUES("4", 10, 15);
insert into `room` (`room_num`, `seats_rows`, `seats_cols`) VALUES("5", 10, 15);
insert into `room` (`room_num`, `seats_rows`, `seats_cols`) VALUES("6", 12, 15);

CREATE TABLE `session` (
`id` INT NOT NULL AUTO_INCREMENT,
`session_time` TIME NOT NULL,
`room_id` INT NOT NULL,
`movie_id` INT NOT NULL,
INDEX `movie_fk_idx` (`movie_id` ASC),
INDEX `room_fk_idx` (`room_id` ASC),
CONSTRAINT `movie_fk`
FOREIGN KEY (`movie_id`)
REFERENCES `bookyst`.`movies` (`id`)
ON DELETE CASCADE
ON UPDATE NO ACTION,
CONSTRAINT `room_fk`
FOREIGN KEY (`room_id`)
REFERENCES `bookyst`.`room` (`id`)
ON DELETE CASCADE
ON UPDATE NO ACTION,
primary key (`id`));

insert into `session` (`session_time`, `movie_id`, `room_id`)
select "12:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather" and room.room_num = 4;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "15:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather" and room.room_num = 4;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "18:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather" and room.room_num = 4;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "21:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather" and room.room_num = 4;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "13:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 3" and room.room_num = 1;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "16:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 3" and room.room_num = 1;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "19:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 3" and room.room_num = 1;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "22:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 3" and room.room_num = 1;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "14:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 2" and room.room_num = 2;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "17:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 2" and room.room_num = 2;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "20:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 2" and room.room_num = 2;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "23:00:00",movies.id,room.id from movies,room
where movies.movie_name = "The Godfather 2" and room.room_num = 2;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "11:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo" and room.room_num = 3;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "14:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo" and room.room_num = 3;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "17:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo" and room.room_num = 3;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "20:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo" and room.room_num = 3;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "10:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 2" and room.room_num = 6;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "13:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 2" and room.room_num = 6;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "16:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 2" and room.room_num = 6;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "19:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 2" and room.room_num = 6;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "09:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 3" and room.room_num = 5;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "12:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 3" and room.room_num = 5;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "15:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 3" and room.room_num = 5;
insert into `session` (`session_time`, `movie_id`, `room_id`)
select "18:00:00",movies.id,room.id from movies,room
where movies.movie_name = "Rambo 3" and room.room_num = 5;

CREATE TABLE `tickets` (
`session_id` INT NOT NULL,
`chair_num` INT NOT NULL,
INDEX `session_fk_idx` (`session_id` ASC),
CONSTRAINT `session_fk`
FOREIGN KEY (`session_id`)
REFERENCES `bookyst`.`session` (`id`)
ON DELETE CASCADE
ON UPDATE NO ACTION,
primary key (`session_id`, `chair_num`));
