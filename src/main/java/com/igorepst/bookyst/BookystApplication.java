package com.igorepst.bookyst;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@SpringBootApplication
public class BookystApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookystApplication.class, args);
	}

	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
		return new HibernateJpaSessionFactoryBean();
	}
	
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/public/");
		resolver.setSuffix(".html");
		return resolver;
	}

	@Bean
	public WebMvcConfigurerAdapter adapter() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addInterceptors(InterceptorRegistry registry) {
				registry.addInterceptor(new HandlerInterceptorAdapter() {
					@Override
					public void postHandle(final HttpServletRequest request, final HttpServletResponse response,
							final Object handler, final ModelAndView modelAndView) {
						// HTTP 1.1
						response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
						// HTTP 1.0
						response.setHeader("Pragma", "no-cache");
						// Proxies
						response.setDateHeader("Expires", 0);
					}
				});
			}
		};
	}

}
