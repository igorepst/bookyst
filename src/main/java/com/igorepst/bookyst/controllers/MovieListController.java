package com.igorepst.bookyst.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.igorepst.bookyst.entity.Movie;
import com.igorepst.bookyst.service.MovieService;

@Controller
public class MovieListController {

	@Autowired
	MovieService movieService;

	@RequestMapping(value = { "/movies" }, method = RequestMethod.GET)
	public @ResponseBody List<Movie> index() {
		return movieService.listAllMovies();
	}

}
