package com.igorepst.bookyst.controllers;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.igorepst.bookyst.entity.MovieSession;
import com.igorepst.bookyst.mappings.IdMapping;
import com.igorepst.bookyst.mappings.MovieSessionMapping;
import com.igorepst.bookyst.service.MovieSessionService;

@RestController
public class MovieSessionController {

	@Autowired
	MovieSessionService movieSessionService;

	@RequestMapping(value = { "/sessions" }, method = RequestMethod.POST)
	public @ResponseBody List<MovieSessionMapping> getSessionsForMovie(
			@RequestBody(required = true) IdMapping movieId) {
		List<MovieSession> sessions = movieSessionService.getSessionsForMovie(movieId.getId());
		Function<MovieSession, MovieSessionMapping> mappingFunc = new Function<MovieSession, MovieSessionMapping>() {

			@Override
			public MovieSessionMapping apply(MovieSession movieSession) {
				return new MovieSessionMapping(movieSession);
			}
		};
		return sessions.stream().map(mappingFunc).collect(Collectors.toList());
	}

}
