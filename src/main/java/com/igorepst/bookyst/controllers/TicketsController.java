package com.igorepst.bookyst.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.igorepst.bookyst.entity.Room;
import com.igorepst.bookyst.entity.Ticket;
import com.igorepst.bookyst.mappings.IdMapping;
import com.igorepst.bookyst.mappings.TicketsMapping;
import com.igorepst.bookyst.service.TicketService;

@RestController
public class TicketsController {

	@Autowired
	TicketService ticketService;

	@RequestMapping(value = { "/occupied" }, method = RequestMethod.POST)
	public @ResponseBody TicketsMapping getOccupiedTickets(@RequestBody(required = true) IdMapping sessionId) {
		List<Ticket> tickets = ticketService.getOccupiedTickets(sessionId.getId());
		if(tickets.size()==0){
			return new TicketsMapping();
		}
		Function<Ticket, Integer> mappingFunc = new Function<Ticket, Integer>() {

			@Override
			public Integer apply(Ticket t) {
				return t.getChairNum();
			}
		};
		Integer[] chairs = Arrays.stream(tickets.toArray(new Ticket[tickets.size()])).map(mappingFunc)
				.toArray(Integer[]::new);
		Room room = tickets.get(0).getMovieSession().getRoom();
		return new TicketsMapping(sessionId.getId(), chairs, room.getSeatsColumnsNumber(), room.getSeatsRowsNumber());
	}

	@RequestMapping(value = { "/buy" }, method = RequestMethod.POST)
	public void buyTickets(@RequestBody(required = true) TicketsMapping tickets) {
		ticketService.buyTickets(tickets.getId(), tickets.getChairs());
	}

}
