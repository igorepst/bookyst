package com.igorepst.bookyst.dao;

import java.util.List;

import com.igorepst.bookyst.entity.Movie;

public interface MovieDao {

	List<Movie> listAllMovies();
}
