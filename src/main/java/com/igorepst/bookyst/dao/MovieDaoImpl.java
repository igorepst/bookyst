package com.igorepst.bookyst.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.igorepst.bookyst.entity.Movie;

@Repository("movieDao")
public class MovieDaoImpl extends AbstractDao<Integer, Movie> implements MovieDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Movie> listAllMovies() {
		Criteria criteria = createEntityCriteria();
		criteria.addOrder(Order.asc("movieName"));
		return criteria.list();
	}

}
