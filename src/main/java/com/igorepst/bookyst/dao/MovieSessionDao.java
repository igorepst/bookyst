package com.igorepst.bookyst.dao;

import java.util.List;

import com.igorepst.bookyst.entity.MovieSession;

public interface MovieSessionDao {
	
	List<MovieSession> getSessionsForMovie(Integer movieId);

}
