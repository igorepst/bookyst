package com.igorepst.bookyst.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.igorepst.bookyst.entity.MovieSession;

@Repository("movieSessionDao")
public class MovieSessionDaoImpl extends AbstractDao<Integer, MovieSession> implements MovieSessionDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<MovieSession> getSessionsForMovie(Integer movieId) {
		Criteria criteria = createEntityCriteria();
		criteria.createAlias("movie", "m").add(Restrictions.eq("m.id", movieId));
		criteria.addOrder(Order.asc("sessionTime"));
		return criteria.list();
	}

}
