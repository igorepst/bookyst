package com.igorepst.bookyst.dao;

import java.util.List;

import com.igorepst.bookyst.entity.Ticket;

public interface TicketDao {

	List<Ticket> getOccupiedTickets(Integer sessionId);
	
	void buyTickets(Integer sessionId, Integer[] chairNums);
}
