package com.igorepst.bookyst.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.igorepst.bookyst.entity.MovieSession;
import com.igorepst.bookyst.entity.Room;
import com.igorepst.bookyst.entity.Ticket;
import com.igorepst.bookyst.entity.TicketPK;

@Repository("ticketDao")
public class TicketDaoImpl extends AbstractDao<TicketPK, Ticket> implements TicketDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Ticket> getOccupiedTickets(Integer sessionId) {
		Criteria criteria = createEntityCriteria();
		criteria.addOrder(Order.asc("chairNum"));
		criteria.createAlias("movieSession", "s").add(Restrictions.eq("s.id", sessionId));
		List<Ticket> result = criteria.list();
		if (result.isEmpty()) {
			MovieSession movieSession = (MovieSession) getSession().get(MovieSession.class, sessionId);
			if (movieSession == null) {
				throw new RuntimeException("Cannot find movie session with id=" + sessionId);
			}
			Ticket ticket = new Ticket();
			ticket.setTicketPK(new TicketPK(sessionId, 0));
			ticket.setMovieSession(movieSession);
			result.add(ticket);
		}
		return result;
	}

	@Override
	public void buyTickets(Integer sessionId, Integer[] chairNums) {
		if (chairNums.length == 0) {
			return;
		}
		MovieSession movieSession = (MovieSession) getSession().get(MovieSession.class, sessionId);
		if (movieSession == null) {
			throw new RuntimeException("Cannot find movie session with id=" + sessionId);
		}
		Room room = movieSession.getRoom();
		// Remove duplicates
		Set<Integer> ticketsSet = Arrays.stream(chairNums).collect(Collectors.toSet());
		Function<Integer, Ticket> mapFunc = new Function<Integer, Ticket>() {

			@Override
			public Ticket apply(Integer t) {
				Ticket ticket = new Ticket();
				ticket.setTicketPK(new TicketPK(sessionId, t));
				return ticket;
			}
		};
		List<Ticket> ticketsList = ticketsSet.stream()
				.filter(p -> p > 0 && p <= room.getSeatsRowsNumber() * room.getSeatsColumnsNumber()).map(mapFunc)
				.collect(Collectors.toList());
		Session session = openSession();
		Transaction transaction = session.getTransaction();
		transaction.setTimeout(3);
		try {
			transaction.begin();
			for (Ticket ticket : ticketsList) {
				session.persist(ticket);
			}
			transaction.commit();
		} catch (RuntimeException e) {
			transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

}
