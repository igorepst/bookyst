package com.igorepst.bookyst.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "room")
public class Room {
	
	@Id
	private Integer id;

	@Column (name = "room_num")
	private Integer roomNumber;
	
	@Column (name = "seats_rows")
	private Integer seatsRowsNumber;
	
	@Column (name = "seats_cols")
	private Integer seatsColumnsNumber;

	public Integer getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(Integer roomNumber) {
		this.roomNumber = roomNumber;
	}

	public Integer getSeatsRowsNumber() {
		return seatsRowsNumber;
	}

	public void setSeatsRowsNumber(Integer seatsRowsNumber) {
		this.seatsRowsNumber = seatsRowsNumber;
	}

	public Integer getSeatsColumnsNumber() {
		return seatsColumnsNumber;
	}

	public void setSeatsColumnsNumber(Integer seatsColumnsNumber) {
		this.seatsColumnsNumber = seatsColumnsNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


}
