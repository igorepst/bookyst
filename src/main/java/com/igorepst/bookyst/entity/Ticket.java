package com.igorepst.bookyst.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tickets")
public class Ticket {
	
	@EmbeddedId
	private TicketPK ticketPK;
	
	@ManyToOne
	@JoinColumn(name = "session_id", insertable = false, updatable = false)
	private MovieSession movieSession;
	
	@Column(name = "chair_num", insertable = false, updatable = false)
	private Integer chairNum;

	public TicketPK getTicketPK() {
		return ticketPK;
	}

	public void setTicketPK(TicketPK ticketPK) {
		this.ticketPK = ticketPK;
	}

	public MovieSession getMovieSession() {
		return movieSession;
	}

	public void setMovieSession(MovieSession movieSession) {
		this.movieSession = movieSession;
	}

	public Integer getChairNum() {
		return chairNum;
	}

	public void setChairNum(Integer chairNum) {
		this.chairNum = chairNum;
	}

}
