package com.igorepst.bookyst.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TicketPK implements Serializable {

	private static final long serialVersionUID = 7756484700949556433L;

	public TicketPK() {
	}

	@Column(name = "chair_num")
	protected Integer chairNum;

	@Column(name = "session_id")
	protected Integer sessionId;

	public TicketPK(Integer sessionId, Integer chairNum) {
		this.sessionId = sessionId;
		this.chairNum = chairNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chairNum == null) ? 0 : chairNum.hashCode());
		result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof TicketPK)) {
			return false;
		}
		TicketPK other = (TicketPK) obj;
		if (chairNum == null) {
			if (other.chairNum != null) {
				return false;
			}
		} else if (!chairNum.equals(other.chairNum)) {
			return false;
		}
		if (sessionId == null) {
			if (other.sessionId != null) {
				return false;
			}
		} else if (!sessionId.equals(other.sessionId)) {
			return false;
		}
		return true;
	}

}
