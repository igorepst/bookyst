package com.igorepst.bookyst.mappings;

public class IdMapping {
	
	private Integer id;

	public IdMapping() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}