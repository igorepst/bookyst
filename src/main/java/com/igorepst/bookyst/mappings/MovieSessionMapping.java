package com.igorepst.bookyst.mappings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.igorepst.bookyst.entity.MovieSession;

public class MovieSessionMapping {
	
	private Integer id;
	
	private String startTime;
	
	private static final DateFormat timeFormatter = new SimpleDateFormat("HH:mm");
	
	public MovieSessionMapping(){}
	
	public MovieSessionMapping(MovieSession movieSession){
		this.id = movieSession.getId();
		startTime = timeFormatter.format(movieSession.getSessionTime()); 
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

}
