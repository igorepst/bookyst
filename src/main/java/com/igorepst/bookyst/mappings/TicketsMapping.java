package com.igorepst.bookyst.mappings;

public class TicketsMapping {
	
	private Integer id;
	
	private Integer[] chairs;
	
	private Integer cols;
	
	private Integer rows;
	
	public TicketsMapping(){}
	
	public TicketsMapping(Integer id, Integer[] chairs, Integer cols, Integer rows){
		this.id = id;
		this.chairs = chairs;
		this.cols = cols;
		this.rows = rows;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer[] getChairs() {
		return chairs;
	}

	public void setChairs(Integer[] chairs) {
		this.chairs = chairs;
	}

	public Integer getCols() {
		return cols;
	}

	public void setCols(Integer cols) {
		this.cols = cols;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

}
