package com.igorepst.bookyst.service;

import java.util.List;

import com.igorepst.bookyst.entity.Movie;

public interface MovieService {
	
	List<Movie> listAllMovies();

}
