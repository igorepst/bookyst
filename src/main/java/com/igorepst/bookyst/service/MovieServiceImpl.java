package com.igorepst.bookyst.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.igorepst.bookyst.dao.MovieDao;
import com.igorepst.bookyst.entity.Movie;

@Service("movieService")
@Transactional
public class MovieServiceImpl implements MovieService{
	
	@Autowired
	private MovieDao movieDao;

	@Override
	public List<Movie> listAllMovies() {
		return movieDao.listAllMovies();
	}

}
