package com.igorepst.bookyst.service;

import java.util.List;

import com.igorepst.bookyst.entity.MovieSession;

public interface MovieSessionService {

	List<MovieSession> getSessionsForMovie(Integer movieId);
}
