package com.igorepst.bookyst.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.igorepst.bookyst.dao.MovieSessionDao;
import com.igorepst.bookyst.entity.MovieSession;

@Service("movieSessionService")
@Transactional
public class MovieSessionServiceImpl implements MovieSessionService{
	
	@Autowired
	private MovieSessionDao movieSessionDao;

	@Override
	public List<MovieSession> getSessionsForMovie(Integer movieId) {
		return movieSessionDao.getSessionsForMovie(movieId);
	}

}
