package com.igorepst.bookyst.service;

import java.util.List;

import com.igorepst.bookyst.entity.Ticket;

public interface TicketService {

	List<Ticket> getOccupiedTickets(Integer sessionId);
	
	void buyTickets(Integer sessionId, Integer[] chairNums);
}
