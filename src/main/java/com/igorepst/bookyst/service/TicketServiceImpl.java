package com.igorepst.bookyst.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.igorepst.bookyst.dao.TicketDao;
import com.igorepst.bookyst.entity.Ticket;

@Service("ticketService")
@Transactional
public class TicketServiceImpl implements TicketService {
	
	@Autowired
	private TicketDao ticketDao;

	@Override
	public List<Ticket> getOccupiedTickets(Integer sessionId) {
		return ticketDao.getOccupiedTickets(sessionId);
	}

	@Override
	public void buyTickets(Integer sessionId, Integer[] chairNums) {
		ticketDao.buyTickets(sessionId, chairNums);
	}

}
